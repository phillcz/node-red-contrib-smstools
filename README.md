Listen and send SMS messages over smstools3 daemon. 
Development is still on early stage so do not be surprised if your hardware 
decides to catch on fire.

### Pre-requisite
 - Install and configure `smstools` server on your machine (<http://smstools3.kekekasvi.com/>).
 - Make sure your `node-red` instance has read permissions in 
all of `/var/spool/sms/` directories and write permissions in
`/var/spool/sms/outgoing` directory.

### Installation
 Install this package globally or to your node-red directory:
```npm install node-red-contrib-smstools```

### Usage
The `sms-in` block will trigger once a message is received. Message object looks like this:
```javascript
{
    "sent": 1457110722000, // Time message was sent (unix timestamp)
    "received": 1457110722000,  // Time received was sent (unix timestamp)
    "topic": 37061234567, // Phone number of a sender
    "payload": "Hello, world!" // Message contents
}
```

To send a message, pass the `msg.topic` and `msg.payload` that corresponds
to recipient's phone number and content. If payload contains only latin
characters ISO encoding will be used. UCS2 format is used otherwise.