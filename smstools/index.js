// ============================================================================
// -  Copyright (C) 2016 Aedilis UAB.  All rights reserved.
// -
// -  Redistribution and use in source and binary forms, with or without
// -  modification, are permitted provided that the following conditions
// -  are met:
// -  1. Redistributions of source code must retain the above copyright
// -     notice, this list of conditions and the following disclaimer.
// -  2. Redistributions in binary form must reproduce the above
// -     copyright notice, this list of conditions and the following
// -     disclaimer in the documentation and/or other materials
// -     provided with the distribution.
// -
// -  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
// -  ``AS IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
// -  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
// -  A PARTICULAR PURPOSE ARE DISCLAIMED.  IN NO EVENT SHALL ANY
// -  CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
// -  EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
// -  PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
// -  PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY
// -  OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
// -  NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
// -  SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
// ============================================================================

module.exports = function (RED) {
    "use strict";
    var sms = require("./smstools");
    var watch = require("watch");
    var monitors = {};

    function smsIn(config) {
        RED.nodes.createNode(this, config);
        var node = this;

        watch.createMonitor('/var/spool/sms/incoming', function (monitor) {
            node.monitor = monitor;
            monitor.on("created", function (f, stat) {
                try {
                    var res = sms.parse(f);
                    node.send(res);
                } catch (err) {
                    node.error("Failed to parse message: " + f);
                }
            });
        });

        node.on('close', function (done) {
            node.monitor.stop(); // Stop watching
            node.status({});
            done();
        });
    }
    RED.nodes.registerType("sms-in", smsIn);

    function smsOut(config) {
        RED.nodes.createNode(this, config);

        this.name = config.name;
        this.topic = config.topic;
        var node = this;

        node.on('input', function (msg) {
            if ('sent' in msg) {
                delete msg.sent;
            }
            if ('received' in msg) {
                delete msg.received;
            }

            // Use the default phone number if not set in message
            if (!('topic' in msg) || msg.topic.length === 0) {
                msg.topic = node.topic;
            }

            var content = sms.stringify(msg);
            var fs = require('fs');
            var id = "node-" + Date.now();
            var out_filename = "/var/spool/sms/outgoing/" + id;

            // Monitor if message was sent
            watch.createMonitor('/var/spool/sms/sent', function (monitor) {
                monitors[id + "-sent"] = monitor;
                monitor.on("created", function (f, stat) {
                    if (f === '/var/spool/sms/sent/' + id) {
                        monitors[id + "-sent"].stop();
                        node.status({
                            fill: "green",
                            shape: "dot",
                            text: "sent"
                        });
                    }
                });
            });

            // Monitor if message sending failed
            watch.createMonitor('/var/spool/sms/failed', function (monitor) {
                monitors[id + "-failed"] = monitor;
                monitor.on("created", function (f, stat) {
                    if (f === '/var/spool/sms/failed/' + id) {
                        monitors[id + "-failed"].stop();
                        node.status({
                            fill: "red",
                            shape: "ring",
                            text: "failed"
                        });

                        var parsed = sms.parse(f);
                        if (parsed !== null && 'fail_reason' in parsed) {
                            node.error("Failed to send message: " + parsed.fail_reason);
                        } else {
                            node.error("Failed to send message");
                        }
                    }
                });
            });

            fs.writeFile(out_filename, content, function (err) {
                if (err) {
                    node.error(err);
                } else {
                    node.status({
                        fill: "blue",
                        shape: "dot",
                        text: "sending"
                    });
                }
            });
        });

        node.on('close', function (done) {
            for (var key in monitors) {
                monitors[key].stop();
            }
            node.status({});
            done();
        });
    }
    RED.nodes.registerType("sms-out", smsOut);
};
